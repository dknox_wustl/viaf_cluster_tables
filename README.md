This repository contains some Bash shell scripts, XSLT stylesheets, and Python scripts
used to process VIAF 'native' XML cluster files found at https://viaf.org/viaf/data/.

The workflow begins in the `extraction` directory. The README in that directory 
documents the steps from downloading a large viaf-NNNNNNNN-clusters.xml.gz file through
extracting four CSV files into the `tables` directory: `expression.csv`, `work.csv`, 
`workexpression.csv`, and `worktitles.csv`.

The `notebooks/prep` directory contains Jupyter Notebooks with Python code.

