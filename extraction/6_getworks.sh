dir=$(basename $(dirname $0));
cd $dir

mkdir -p ../tables/raw

echo "Extracting work data to ../tables/raw/work.tsv"

cat download/work_short.xml | parallel 'printf "%s" {} | xmlstarlet tr xsl/work.xsl' >../tables/raw/work.tsv

# append the few that are too long for the xml to be passed on the 
# command line
xmlstarlet tr xsl/work_long.xsl download/work_long.xml >>../tables/raw/work.tsv

echo "Done"

