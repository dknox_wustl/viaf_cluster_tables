# for the sake of freeing up disk space, remove the contents of the "download" directory.

dir=$(basename $(dirname $0));
cd $dir

while true; do
  echo;
  echo "This will delete *.gz and *.xml files in /download.";
  read -p "Are you sure you want to do that? [y/n] " yn
  case $yn in
    [Yy]* ) rm download/*.gz download/*.xml; break;;
    [Nn]* ) echo "No problem."; exit;;
    * ) echo "Please answer yes or no.";;
  esac
done



