#!/usr/bin/env python

import os
import sys
i = 0

basedir = os.path.join(os.path.dirname(sys.argv[0]), "download")
workpath = os.path.join(basedir, "work.xml")
longpath = os.path.join(basedir, "work_long.xml")
shortpath = os.path.join(basedir, "work_short.xml")

with open(workpath) as f, \
  open(longpath, "w") as wl, \
  open(shortpath, "w") as ws:
    wl.write("""<Clusters xmlns="http://viaf.org/viaf/terms#" >""")
    l = f.readline()
    wl.write(l)
    ws.write(l)
    l = f.readline()
    while l:
        if len(l)>100000:
            wl.write(l)
        else:
            ws.write(l)
        l = f.readline()

        i += 1
        if i % 100000 == 0:
            print(i, end=" ")
            sys.stdout.flush()
        if i % 10000 == 0:
            print(".", end=" ")
            sys.stdout.flush()
    wl.write("""</Clusters>""")
