dir=$(basename $(dirname $0));
cd $dir

mkdir -p ../tables/raw

echo "Extracting work-expression data to ../tables/raw/workexpression.tsv"


cat download/work_short.xml | parallel 'printf "%s" {} | xmlstarlet tr xsl/workexpression.xsl' >../tables/raw/workexpression.tsv

# append the few that are too long for the xml to be passed on the 
# command line

xmlstarlet tr xsl/workexpression.xsl download/work_long.xml >>../tables/raw/workexpression.tsv

echo "Done"
