dir=$(basename $(dirname $0));
cd $dir

mkdir -p ../tables/raw

echo "Extracting work titles to ../tables/raw/worktitles.tsv"

cat download/work_short.xml | parallel 'printf "%s" {} | xmlstarlet tr xsl/worktitles.xsl' >../tables/raw/worktitles.tsv

# append the few that are too long for the xml to be passed on the 
# command line
xmlstarlet tr xsl/worktitles_long.xsl download/work_long.xml >>../tables/raw/worktitles.tsv

echo "Done"

