#!/usr/bin/env python
from lxml import etree
import gzip
import os
import sys

viafns = "http://viaf.org/viaf/terms#"
basedir = os.path.join(os.path.dirname(sys.argv[0]), "download")
clusterpath = os.path.join(basedir, "viaf-20180401-clusters.xml.gz")
personpath = os.path.join(basedir, "../../tables/personal.tsv")

def process_line(line):
    """Turn a VIAF cluster line for a personal record 
    into a string consisting of TSV rows with authorid and name form."""
    xstring = line.decode("utf-8").strip("0123456789\t")
    root = etree.XML(xstring)
    authid = root.find("{{{}}}viafID".format(viafns)).text
    namespath = "{{{}}}mainHeadings/{{{}}}data/{{{}}}text".format(
        viafns,viafns,viafns)
    names = [e.text for e in root.findall(namespath)]
    return "".join(["{}\t{}\n".format(authid, name) 
                    for name in names])

# Read from cluster file, find personal records, 
# and write viafid - name-form matches to `personal.tsv`.

with open(personpath, "w") as personal, gzip.open(clusterpath) as f:
    line = f.readline()
    while line:
        if "<nameType>Personal" in line.decode("utf-8"):
            personal.write(process_line(line))
        line = f.readline()

