dir=$(basename $(dirname $0));
cd ${dir}/download

echo "Extracting expressions to expression.xml"
cat uniformtitle.xml | parallel --pipe 'grep "UniformTitleExpression"' >expression.xml

echo "Extracting works to work.xml"
cat uniformtitle.xml | parallel --pipe 'grep "UniformTitleWork"' >work.xml
