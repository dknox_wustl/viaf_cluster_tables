<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:hdw="http://hdw.artsci.wustl.edu/ns"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:viaf="http://viaf.org/viaf/terms#"
    exclude-result-prefixes="xs" version="2.0">

    <xsl:output omit-xml-declaration="yes"/>

    <xsl:function name="hdw:yearclean">
        <xsl:param name="yearstr"/>
        <xsl:value-of select="replace($yearstr, '[^0-9]', '')"/>
    </xsl:function>

    <xsl:template match="/">
        <xsl:apply-templates
            select="viaf:Clusters/viaf:VIAFCluster[viaf:nameType = 'UniformTitleWork' and count(viaf:titles/viaf:expression/viaf:lang) &gt; 1]"
        />
    </xsl:template>

    <xsl:template match="viaf:VIAFCluster[viaf:nameType = 'UniformTitleWork']">
        <xsl:value-of select="viaf:viafID"/>
        
        <xsl:text>&#x09;</xsl:text>
        
        <xsl:value-of select="normalize-space(viaf:mainHeadings/viaf:data[1]/viaf:text)"/>

        <xsl:text>&#x09;</xsl:text>
        <xsl:value-of select="viaf:languageOfEntity/viaf:data/viaf:text"/>

        <xsl:text>&#x09;</xsl:text>
        <xsl:value-of select="count(viaf:titles/viaf:expression/viaf:lang)"/>

        <xsl:text>&#x09;</xsl:text>

        <xsl:apply-templates select="viaf:titles/viaf:expression"/>

        <xsl:text>&#x0a;</xsl:text>
    </xsl:template>

    <xsl:template match="viaf:titles/viaf:expression">
        <xsl:value-of select="viaf:lang"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="hdw:yearclean(viaf:datafield[@tag = '400']/viaf:subfield[@code = 'f'])"/>
        <xsl:text>; </xsl:text>
    </xsl:template>

    <xsl:template match="*"/>

</xsl:stylesheet>
