<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:viaf="http://viaf.org/viaf/terms#"
    exclude-result-prefixes="xs" version="1.0">

    <xsl:output omit-xml-declaration="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates select="//viaf:VIAFCluster//viaf:titles/viaf:expression"/>
    </xsl:template>

    <xsl:template match="viaf:expression">

        <!-- work VIAF ID-->

        <xsl:value-of select="../../viaf:viafID"/>
        <xsl:text>&#x09;</xsl:text>

        <!-- expression VIAF ID-->
        <xsl:value-of select="@id"/>
        <xsl:text>&#x09;</xsl:text>

        <!-- spelled-out language -->
        <xsl:value-of select="viaf:lang"/>
        <xsl:text>&#x09;</xsl:text>

        <!-- author name -->
        <xsl:value-of select="viaf:datafield[@tag = '400']/viaf:subfield[@code = 'a']"/>
        <xsl:text>&#x09;</xsl:text>

        <!-- author dates -->
        <xsl:value-of select="viaf:datafield[@tag = '400']/viaf:subfield[@code = 'd']"/>
        <xsl:text>&#x09;</xsl:text>

        <!-- title -->
        <xsl:value-of select="viaf:datafield[@tag = '400']/viaf:subfield[@code = 't']"/>
        <xsl:if test="position() != last()">
            <xsl:text>&#x0a;</xsl:text>
        </xsl:if>

    </xsl:template>

</xsl:stylesheet>
