<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:viaf="http://viaf.org/viaf/terms#"
    exclude-result-prefixes="xs" version="1.0">

    <xsl:output  omit-xml-declaration="yes"/>
    
    <xsl:template match="/">
        <xsl:apply-templates select="//viaf:VIAFCluster"/>
    </xsl:template>

    <xsl:template match="viaf:VIAFCluster">
        <xsl:value-of select="viaf:viafID"/>
        <xsl:text>&#x09;</xsl:text>


        <!-- edition year -->
        <xsl:value-of select="translate(
            viaf:mainHeadings/viaf:mainHeadingEl/viaf:datafield[@tag='100']/viaf:subfield[@code='f'], '[)]', '')"/>
        <xsl:text>&#x09;</xsl:text>
        
        <!-- language code -->
        <xsl:value-of select="viaf:languageOfEntity/viaf:data/viaf:text"/>
        <xsl:text>&#x09;</xsl:text>
        
        <!-- spelled-out language -->
        <xsl:value-of select="viaf:mainHeadings/viaf:mainHeadingEl/viaf:datafield[@tag='100']/viaf:subfield[@code='l']"/>
        <xsl:text>&#x09;</xsl:text>
        
        <!-- author name -->
        <xsl:value-of select="viaf:mainHeadings/viaf:mainHeadingEl/viaf:datafield[@tag='100']/viaf:subfield[@code='a']"/>
        <xsl:text>&#x09;</xsl:text>
        

        <!-- author viaf -->
        <xsl:value-of select="viaf:mainHeadings/viaf:mainHeadingEl/viaf:datafield[@tag='100']/viaf:subfield[@code='0']"/>
        <xsl:text>&#x09;</xsl:text>
        
        <!-- work title -->
        <xsl:value-of select="viaf:mainHeadings/viaf:mainHeadingEl/viaf:datafield[@tag='100']/viaf:subfield[@code='t']"/>
        <xsl:text>&#x09;</xsl:text>
        
        <!-- expression first title -->
        <xsl:value-of select="viaf:x400s/viaf:x400[1]/viaf:datafield[@tag='400']/viaf:subfield[@code='t']"/>

    </xsl:template>

    <xsl:template match="viaf:mainHeadings">
        <xsl:value-of select="viaf:viafID"/>
        <xsl:text>&#x0a;</xsl:text>
    </xsl:template>
    

</xsl:stylesheet>
