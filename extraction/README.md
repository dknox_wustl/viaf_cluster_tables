This directory contains shell scripts for downloading VIAF cluster XML and extracting
just the work and expression records. The scripts have been run on
Ubuntu Linux. They assume GNU Parallel has been installed, to make use
of multiple cores.

STEP 0

Get the clustered data as an XML file. There's more than one way to 
do it; this shell script won't work without editing the URL to something current, but 
it's perhaps a convenient reminder of the form of the URL:

    ./0_download.sh

The idea is to put a big (13GB) gzipped file in `extraction/download`

Each line is a self-contained XML record. It's a lot of data, but no
single record is all that big. My general strategy for the sake of
speed and simplicity is to use shell tools to manage iterating over
the lines and combining outputs, and to use short XSLT 1.0 stylesheets
to extract flat fields from each record.

STEP 1

This script filters just the UniformTitle records into
`uniformtitle.xml` (9.1GB uncompressed).

     ./1_extract_uniform_title.sh

STEP 2

This splits off UniformTitleExpression records into `expression.xml` 
and UniformTitleWork records into `work.xml`:

     ./2_extract_expression.sh

STEP 3

A series of scripts create derivative CSV files. Getting a CSV file
for the expressions is straightforward:

     ./3_getexpressions.sh → ../tables/raw/expression.tsv

Command-line tools break down with work.xml because some of the lines
are too long. My solution is clunky, but for now I get around this by
using a short Python script to split `work.xml` into `work_short.xml`
and `work_long.xml`. Later I process these separately and combine the
results.  Because `work_long` is not in itself all that big, and I
want to pass a filename rather than raw XML to the XSLT processor, I
wrap the clusters in `work_long` to make it a single valid XML file,
although for the sake of speed and simplicity I still prefer
processing `work_short` line by line.

STEPS 4 -7

     ./4_splitwork.py 
     ./5_getworkexpression.sh → ../tables/raw/workexpression.tsv
     ./6_getworks.sh → ../tables/raw/work.tsv
     ./getworktitles.sh → ../tables/raw/worktitles.tsv


CLEANUP

There is an optional script to clean up the /download directory
just to free up disk space. It asks for confirmation before
deleting *.gz and *.xml files.

    ./99_cleanup.sh
