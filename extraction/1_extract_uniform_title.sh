dir=$(basename $(dirname $0));
cd ${dir}/download
nohup time zcat viaf-*.xml.gz | 
  parallel --pipe grep "UniformTitle" | 
  parallel --pipe 'sed "s/^[0-9]\+\s\+//"' >uniformtitle.xml &
