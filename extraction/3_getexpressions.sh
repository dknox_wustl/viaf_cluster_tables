dir=$(basename $(dirname $0));
cd $dir

mkdir -p ../tables/raw

echo "Extracting expression data to ../tables/raw/expression.tsv"

cat download/expression.xml | parallel 'printf "%s" {} | xmlstarlet tr xsl/expression.xsl' >../tables/raw/expression.tsv

echo "Done"
